﻿using Nemerle;
using System;

namespace NBox.TryNemerle.Core.Entities
{
  [Record, Serializable]
  public class LimitSet
  {
    CompileMemoryUsageBytes : long;
    CompileTimeMs           : long;
    ExecuteMemoryUsageBytes : long;
    ExecuteTimeMs           : long;
    DirectorySizeBytes      : long;
    ExecutionFileSizeBytes  : long;
    CodeSizeChars           : int;
    InputSizeChars          : int;
    OutputSizeChars         : int;
    LifeTimeMs              : long;
  }
}

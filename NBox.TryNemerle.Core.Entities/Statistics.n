﻿using Nemerle;
using System;

namespace NBox.TryNemerle.Core.Entities
{
  [Record, Serializable]
  public class Statistics
  {
    CompileMemoryUsageBytes : long;
    CompileTimeMs           : long;
    ExecuteMemoryUsageBytes : long;
    ExecuteTimeMs           : long;
    TotalCpuUsageMs         : long;
  }
}

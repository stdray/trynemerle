﻿using Nemerle;
using System;

namespace NBox.TryNemerle.Core.Entities
{
  [Flags, Serializable]
  public enum CompilerLogEvents
  {
    | Message = 1 << 0
    | Warning = 1 << 1
    | Error   = 1 << 2
  }
}

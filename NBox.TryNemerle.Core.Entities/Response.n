﻿using Nemerle;
using System;

namespace NBox.TryNemerle.Core.Entities
{
  [Record, Serializable]
  public variant Response
  {
    Id            : Guid;
    Statistics    : Statistics;
    CompileOutput : string;
    ExecuteOutput : string;
    
    | Success
    | CompileFail { exception : Exception }
    | ExecuteFail { exception : Exception }
  }
}

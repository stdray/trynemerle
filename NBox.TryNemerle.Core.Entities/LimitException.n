﻿using Nemerle;
using System;

namespace NBox.TryNemerle.Core.Entities
{
  [Record, Serializable]
  public class LimitException : Exception
  {
    LimitType : LimitType;
  }
}

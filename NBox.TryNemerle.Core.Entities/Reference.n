﻿using Nemerle;
using System;
using System.Reflection;

namespace NBox.TryNemerle.Core.Entities
{
  [Serializable]
  public variant Reference
  {
    | GAC    { assembly  : AssemblyName }
    | Nuget  { packageId : string; version : string; targetFramework : string }
    | Binary { data : array[byte]; fileName : string }
  }
}

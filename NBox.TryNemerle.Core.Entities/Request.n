﻿using Nemerle;
using System;
using System.Collections.Generic;
using System.Security;

namespace NBox.TryNemerle.Core.Entities
{
  [Record, Serializable]
  public variant Request
  {
    Id                : Guid;
    Permissions       : PermissionSet;
    Limits            : LimitSet;
    References        : List[Reference];
    CompilerLogEvents : CompilerLogEvents;
    
    | Console    { programCode : CodeItem; macroCode : CodeItem; input : string; }
    | Nitra      { programCode : CodeItem; grammarCode : CodeItem; input : string; }
    | NemerleWeb { programCode : CodeItem; }
  }
}

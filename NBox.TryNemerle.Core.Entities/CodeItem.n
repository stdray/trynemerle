﻿using Nemerle;
using System;

namespace NBox.TryNemerle.Core.Entities
{
  [Record, Serializable]
  public class CodeItem
  {
    FileName : string;
    Code     : string;
  }
}

﻿using System;

namespace NBox.TryNemerle.Core.Entities
{
  [Serializable]
  public enum LimitType
  {
    | CompileMemoryUsageBytes
    | CompileTimeMs
    | ExecuteMemoryUsageBytes 
    | ExecuteTimeMs
    | DirectorySizeBytes
    | ExecutionFileSizeBytes
    | CodeSizeChars
    | InputSizeChars
  }
}

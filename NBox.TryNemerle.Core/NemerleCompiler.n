﻿using Nemerle;
using Nemerle.Async;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Compiler;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using NBox.TryNemerle.Core.InternalEntities;
using NBox.TryNemerle.Core.Entities;

namespace NBox.TryNemerle.Core
{
  public class NemerleCompiler
  {
    class Manager : ManagerClass
    {
      public this(output : TextWriter, options : CompilationOptions)
      {
        base(options);
        InitOutput(output);
        InitCompiler();
        LoadExternalLibraries();
      }
    }

    Compile(output : TextWriter, request : CompilerRequest) : void
    {
      def makeSource(item) 
      {
        def filePath = Path.Combine(request.SourcePath, item.FileName);
        File.WriteAllText(filePath, item.Code);
        FileSource(filePath, true, false);
      }
      unless(Directory.Exists <| request.SourcePath)
        _ = Directory.CreateDirectory(request.SourcePath);
      def sources = request.Sources.Map(makeSource);
      def options = CompilationOptions() <-
      {
        CompileToMemory        = false;
        ColorMessages          = false;
        DisableExternalParsers = false;
        DoNotLoadMacros        = false;
        EarlyExit              = true;
        IgnoreConfusion        = true;
        OutputFileName         = Path.GetFileName(request.OutputFile);
        OutputPath             = Path.GetDirectoryName(request.OutputFile);
        Sources                = sources;
        ReferencedLibraries    = request.References;
        MacrosToLoad           = request.MacroReferences;
        LibraryPaths           = request.ReferencePaths;
      }
      Manager(output, options).Run();
    }

  }
}

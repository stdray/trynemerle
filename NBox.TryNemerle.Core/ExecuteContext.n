﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NBox.TryNemerle.Core
{

  public class ExecuteContext : IDisposable
  {
    RequestDir : string;

    public this(baseDir : string, requestId : Guid)
    {
      RequestDir = Path.Combine(baseDir, requestId.ToString()) |> ensureDirExist
    }

    SourceDir : string 
    {
      get {  Path.Combine(RequestDir, "src") |> ensureDirExist }
    }

    OutputDir : string 
    {
      get { Path.Combine(RequestDir, "bin") |> ensureDirExist } 
    }

    ensureDirExist(dir : string) : string 
    {
      unless(Directory.Exists <| dir)
        _ = Directory.CreateDirectory(dir);
      dir;
    }

    public Dispose() : void
    {
      Dispose(true);
    }

    protected override Finalize() : void 
    {
      Dispose(false);
    }

    protected Dispose(disposing : bool) : void
    {
      when(Directory.Exists <| RequestDir)
        _ = Directory.Delete(RequestDir, true);
      when(disposing)
        GC.SuppressFinalize(this);
    }
  }
}

﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace NBox.TryNemerle.Core
{
  [Record]
  public class Sandbox : MarshalByRefObject, IDisposable
  {
    BaseDirectory : string;
    
    #region dispose
    protected Dispose(disposing : bool) : void
    {
      when(disposing)
        GC.SuppressFinalize(this);
    }
    public Dispose() : void
    { 
      throw System.NotImplementedException()
    }
    #endregion
  }
}

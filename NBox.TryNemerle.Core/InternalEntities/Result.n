﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;

namespace NBox.TryNemerle.Core.InternalEntities
{
  [Serializable, Record]
  public variant Result[T, U]
  {
    Output : string;
    
    | Success { value : T }
    | Fail    { exception : Exception }
  }
}

﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using NBox.TryNemerle.Core.Entities;

namespace NBox.TryNemerle.Core.InternalEntities
{
  [Record, Serializable]
  public class CompilerRequest
  {
    SourcePath      : string;
    Sources         : list[CodeItem];
    ReferencePaths  : list[string];
    MacroReferences : list[string];
    References      : list[string];
    OutputFile      : string;
    LogEvents       : CompilerLogEvents;
  }
}

﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Extensions;
using Nemerle.Text;
using Nemerle.Utility;
using NUnit.Framework;
using System;
using SCG = System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using NBox.TryNemerle.Macros.Tests;
using NBox.TryNemerle.Core.Entities;
using NBox.TryNemerle.Core;
using NBox.TryNemerle.Core.InternalEntities;

namespace NBox.TryNemerle.Core.Tests
{
  [TestFixture]
  public class NemerleCompilerTests
  {

    helloWorldProgram : string =
    <#
      using System.Console;

      module Program
      {
        Main() : void
        {
          WriteLine("Hello world!");
          _ = ReadLine();
        }
      }
    #>;

    helloWorldScript : string = 
    <#
      System.Console.WriteLine("Hello world!");
    #>;

    grammarCode : string = 
    <#
syntax module Calc
{
  using PrettyPrint;
  using Outline;
  using TokenNames;
  using StandardSpanClasses;
  using Whitespaces;
  using Identifiers;
  using CStyleComments;

  [StartRule]
  syntax Start = Expression !Any
  {
    Value() : double = Expression.Value();
  }

  extend token IgnoreToken
  {
    | [SpanClass(Comment)] SingleLineComment = SingleLineComment;
    | [SpanClass(Comment)] MultiLineComment;
  }

  regex Number = ['0'..'9']+ ('.' ['0'..'9']*)?;

  syntax Expression
  {
    Value() : double;
    missing Value = double.NaN;

    | Number
      {
        override Value = double.Parse(GetText(this.Number));
      }
    | Add    = Expression sm '+' sm Expression    precedence 10
      {
        override Value = Expression1.Value() + Expression2.Value();
      }
    | Sub    = Expression sm '-' sm Expression    precedence 10
      {
        override Value = Expression1.Value() -  Expression2.Value();
      }
    | Mul    = Expression sm '*' sm Expression    precedence 20
      {
        override Value = Expression1.Value() * Expression2.Value();
      }
    | Div    = Expression sm '/' sm Expression    precedence 20
      {
        override Value = Expression1.Value() / Expression2.Value();
      }
    | Pow    = Expression sm '^' sm Expression    precedence 30 right-associative
      {
        override Value = System.Math.Pow(Expression1.Value(), Expression2.Value());
      }
    | Neg    = '-' Expression    precedence 100
      {
        override Value = -Expression.Value();
      }
    | Plus   = '+' Expression    precedence 100
      {
        override Value = Expression.Value();
      }
    | Rounds = '(' Expression ')'
      {
        override Value = Expression.Value();
      }
  }
}

    #>;

    programCode : string = 
    <#
using Nitra;

using Nemerle.Collections;
using Nemerle.Text;
using Nemerle.Utility;
using Nemerle.Imperative;

using System;
using System.Collections.Generic;
using System.Console;
using System.Linq;

module Program
{
  Main() : void
  {
    def parserHost = ParserHost();
    for (;;)
    {
      Write("input>");
      def input = ReadLine();
      when (string.IsNullOrWhiteSpace(input))
        break;

      def source = SingleLineSourceSnapshot(input);
      def parseResult = Calc.Start(source, parserHost);
      def ast = CalcAst.Start.Create(parseResult);
      WriteLine($"Pretty print: $ast");

      unless (parseResult.IsSuccess)
        foreach(error in parseResult.GetErrors())
        {
          def (line, col) = error.Location.StartLineColumn;
          WriteLine($<#$line:$col: $(error.Message)#>);
        }

      def result = ast.Value();
      WriteLine($"Result: $result");
    }
  }
}

    #>;

    path = @"D:\temp";

    getTempPath() : string 
    {
      path
    }

    simpleRequest(ctx : ExecuteContext, outputFileName : string, codeItem : CodeItem) : CompilerRequest
    {
      CompilerRequest
      (
        sourcePath = ctx.SourceDir,
        references = [ ],
        macroReferences = [ ], 
        referencePaths = [ ],
        outputFile = Path.Combine(ctx.OutputDir, outputFileName),
        logEvents = CompilerLogEvents.Message, 
        sources = [ codeItem ]
      )
    }

    tryCompile(request : CompilerRequest) : void 
    {
      def output = StringWriter();
      def compiler = NemerleCompiler();
      try 
      {
        compiler.Compile(output, request);
      }
      catch
      {
        | _ => Trace.WriteLine(output); throw;
      }
    }

    [Test]
    CompileHelloWorldScript() : void 
    {
      using(ctx = ExecuteContext(getTempPath(), Guid.NewGuid()))
      {
        CodeItem("helloworld.n", helloWorldScript)
        |> simpleRequest(ctx, "HelloWorld.exe", _)
        |> tryCompile;
      }
    }

    [Test]
    CompileHelloWorldProgram() : void 
    {
      using(ctx = ExecuteContext(getTempPath(), Guid.NewGuid()))
      {
        CodeItem("helloworld.n", helloWorldProgram)
        |> simpleRequest(ctx, "HelloWorld.exe", _)
        |> tryCompile;
      }
    }

    [Test]
    CompileNitraCalc() : void 
    {
      using(ctx = ExecuteContext(getTempPath(), Guid.NewGuid()))
      {
        def request = CompilerRequest
        (
          references = [ "Nitra.Runtime.dll", "Nitra.Core.dll" ], 
          macroReferences = [ "Nitra.Compiler.dll" ],
          referencePaths = [ @"C:\Program Files (x86)\JetBrains\Nitra\" ],
          sourcePath = ctx.SourceDir,
          outputFile = Path.Combine(ctx.OutputDir, "Calc.exe"),
          logEvents = CompilerLogEvents.Message, 
          sources = [ CodeItem("Calc.nitra", grammarCode), CodeItem("Main.n", programCode) ]
        );
        tryCompile(request);
      }
    }
  }
}

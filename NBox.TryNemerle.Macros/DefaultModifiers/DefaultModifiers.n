﻿using Nemerle;
using Nemerle.Compiler;
using Nemerle.Compiler.Parsetree;

namespace NBox.TryNemerle.Macros.DefaultModifiers
{
  using RuleUtils;
  using СonditionOperators;

  [MacroUsage(MacroPhase.BeforeInheritance, MacroTargets.Assembly)]
  macro DefaultModifiers() 
  {
    DefaultModifiersImpl.Transform(Macros.Manager())
  }
  module DefaultModifiersImpl {
    public Transform(manager : ManagerClass) : void 
    {
      def typeRules = [
                         TypeRule(cond = !HasAccessModifiers,
                                  eval = SetModifier(NM.Public, _)) 
                       ];
      RunRules(typeRules, AllTypes <| manager);
      def memberRules = [
                           MemberRule(cond = !IsInterfaceMember & !HasAccessModifiers & MemberName >> IsFstUpper, 
                                      eval = SetModifier(NM.Public, _)),
                           MemberRule(cond = !IsInterfaceMember & !HasAccessModifiers & MemberName >> IsFstLower, 
                                      eval = SetModifier(NM.Private, _)),
                           MemberRule(cond = !IsInterfaceMember & !HasAccessModifiers & MemberName >> FstChar >> (_ == '_'), 
                                      eval = SetModifier(NM.Mutable, _)),
                           MemberRule(cond = !HasAccessModifiers & IsCtor, 
                                      eval = SetModifier(NM.Public, _)),
                         ];
      RunRules(memberRules, AllMembers <| manager);
    } 
  }
}

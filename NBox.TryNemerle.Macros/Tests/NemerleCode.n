﻿using Nemerle;
using Nemerle.Collections;
using Nemerle.Compiler;
using Nemerle.Compiler.Parsetree;
using Nemerle.Compiler.Typedtree;
using Nemerle.Text;
using Nemerle.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace NBox.TryNemerle.Macros.Tests
{
  macro NemerleCode(expr : PExpr)
  syntax ("nemerle", expr) 
  {
    <[ $(expr.ToString()) ]>
  }
}
